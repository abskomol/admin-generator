<?php

namespace Tirjok\AdminGenerator\Controllers;

use App\Http\Controllers\Controller;
use Artisan;
use File;
use Illuminate\Http\Request;
use Response;
use Session;
use View;

class ProcessController extends Controller
{
    /**
     * Display generator.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getGenerator()
    {
        View::addNamespace('AdminGenerator', __DIR__ . '/../views');

        return view('AdminGenerator::generator');
    }


    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Response
     */
    public function postGenerator(Request $request)
    {
        $commandArg = $this->getCommandArgs($request);

        try {
            Artisan::call('crud:generate', $commandArg);

            $menus = json_decode(File::get(base_path('resources/admin-generator/menus.json')));

            $name = $commandArg['name'];
            $routeName = isset($commandArg['--route-group']) ? $commandArg['--route-group'] . '/' . snake_case($name, '-') : snake_case($name, '-');

            $menus->menus = array_map(function ($menu) use ($name, $routeName) {
                if ($menu->section == 'Modules') {
                    array_push($menu->items, (object) [
                        'title' => $name,
                        'url' => '/' . $routeName,
                    ]);
                }

                return $menu;
            }, $menus->menus);

            File::put(base_path('resources/admin-generator/menus.json'), json_encode($menus));

            Artisan::call('migrate');
        } catch (\Exception $e) {
            return response($e->getMessage(), 500);
        }

        Session::flash('flash_message', 'Your CRUD has been generated. See on the menu.');

        return redirect('admin/generator');
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getCommandArgs(Request $request)
    {
        $commandArg = [];
        $commandArg['name'] = $request->crud_name;

        $fieldsArray = [];
        $validationsArray = [];
        $uniqueArray = [];
        $indexArray = [];
        $foreignKeys = [];
        $filtersArray = [];

        if ($request->has('fields')) {

            $x = 0;
            foreach ($request->fields as $field) {
                if ($request->fields_required[$x] == 1) {
                    $validationsArray[] = $field;
                }

                $fieldStr = $field . '#' . $request->fields_type[$x];

                if ($request->keys[$x] == 'unsigned') {
                    $fieldStr = $fieldStr . '#unsigned';
                }

                $fieldsArray[] = $fieldStr;

                // Index, Unique, Keys
                if ($request->keys[$x] == 'unique') {
                    $uniqueArray[] = $field . '#unique';
                } elseif ($request->keys[$x] == 'index') {
                    $indexArray[] = $field;
                }

                // Foreign Keys
                if ($request->foreign_keys[$x]) {
                    $foreignKeys[] = $field . '#' . $request->foreign_keys[$x];
                }

                // filterable
                if ($request->filters[$x] === 'like') {
                    $filtersArray[] = $field . '#' . $request->filters[$x];
                } elseif ($request->filters[$x] === '=') {
                    $filtersArray[] = $field;
                }

                $x++;
            }

            $commandArg['--fields'] = implode(";", $fieldsArray);
        }

        if (!empty($filtersArray)) {
            $commandArg['--filters'] = implode(";", $filtersArray);
        }

        if (!empty($foreignKeys)) {
            $commandArg['--foreign-keys'] = implode(",", $foreignKeys);
        }

        if (!empty($validationsArray)) {
            $commandArg['--validations'] = implode("#required;", $validationsArray) . "#required";
        }

        $indexes = '';

        if (!empty($indexArray)) {
            $indexes = implode(',', $indexArray);
        }

        if (!empty($uniqueArray)) {
            $indexes .= ',' . implode(',', $uniqueArray);
        }


        if ($indexes) {
            $commandArg['--indexes'] = $indexes;
        }

        if ($request->has('namespace')) {
            $commandArg['--namespace'] = $request->namespace;
            $commandArg['--view-path'] = strtolower($request->namespace);
            $commandArg['--route-group'] = strtolower($request->namespace);
        }

        $commandArg['--sd'] = $request->sd;
        return $commandArg; // Soft delete
    }
}
