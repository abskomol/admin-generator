<?php

Route::group(['prefix' => 'admin', 'middleware' => ['web']], function () {
    Route::get('generator', ['uses' => 'Tirjok\AdminGenerator\Controllers\ProcessController@getGenerator']);
    Route::post('generator', ['uses' => 'Tirjok\AdminGenerator\Controllers\ProcessController@postGenerator']);
});
