<?php

namespace Tirjok\AdminGenerator;

use App\Http\Middleware\RoleMiddleware;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use File;

class AdminGeneratorServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Perform post-registration booting of services.
     *
     * @param \Illuminate\Routing\Router $router
     */
    public function boot(Router $router)
    {
        $this->publishes([
            __DIR__ . '/publish/Middleware/' => app_path('Http/Middleware'),
        ]);

        $this->publishes([
            __DIR__ . '/publish/migrations/' => database_path('migrations'),
        ]);

        $this->publishes([
            __DIR__ . '/publish/seeds/' => database_path('seeds'),
        ]);

        $this->publishes([
            __DIR__ . '/publish/Model/' => app_path('Models/Admin'),
        ]);

        $this->publishes([
            __DIR__ . '/publish/Services/' => app_path('Services'),
        ]);

        $this->publishes([
            __DIR__ . '/publish/Traits/' => app_path('Traits'),
        ]);

        $this->publishes([
            __DIR__ . '/publish/Repositories/' => app_path('Repositories'),
        ]);

        $this->publishes([
            __DIR__ . '/publish/Controllers/' => app_path('Http/Controllers'),
        ]);

        $this->publishes([
            __DIR__ . '/publish/views/' => base_path('resources/views'),
        ]);

        $this->publishes([
            __DIR__ . '/publish/resources/' => base_path('resources'),
        ]);

        $this->publishes([
            __DIR__ . '/publish/config/crudgenerator.php' => config_path('crudgenerator.php'),
        ]);

        $router->middleware('roles', RoleMiddleware::class);

        include __DIR__ . '/routes.php';

        if (File::exists(base_path('resources/admin-generator/menus.json'))) {
            $menus = json_decode(File::get(base_path('resources/admin-generator/menus.json')));
            view()->share('laravelAdminMenus', $menus);
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands(
            'Tirjok\AdminGenerator\AdminCommand'
        );
    }
}
