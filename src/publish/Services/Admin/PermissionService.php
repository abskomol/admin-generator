<?php 

namespace App\Services\Admin;

use App\Repositories\Admin\PermissionRepository;
use App\Services\BaseService;

class PermissionService extends BaseService
{
    /**
     * @var \App\Repositories\Admin\PermissionRepository
     */
    protected $repository;

    public function __construct(PermissionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \App\Repositories\Repository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function select($columns = ['*'])
    {
        return $this->repository->get($columns);
    }

    /**
     * @param $attribute
     * @param $value
     *
     * @return mixed
     */
    public function findPermissionBy($attribute, $value)
    {
        return $this->repository->findBy($attribute, $value);
    }
}