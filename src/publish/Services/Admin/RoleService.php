<?php

namespace App\Services\Admin;

use App\Repositories\Admin\RoleRepository;
use App\Services\BaseService;

class RoleService extends BaseService
{
    /**
     * @var \App\Repositories\Admin\RoleRepository
     */
    protected $repository;

    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \App\Repositories\Repository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function select($columns = ['*'])
    {
        return $this->repository->get($columns);
    }

    /**
     * Get with relational
     *
     * @param $with
     *
     * @return mixed
     */
    public function getRoleWith($with)
    {
        if (!is_array($with)) {
            $with = [$with];
        }

        return $this->repository->getQueryWith($with);
    }

    /**
     * @param $where
     * @param $with
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function findByWithRelation($where, $with)
    {
        $query = $this->getRoleWith($with)
            ->where($where['attribute'], '=', $where['value']);

        return $query->first();
    }
}