<?php

namespace App\Services\Admin;

class AdminService
{
    /**
     * @var \App\Services\Admin\RoleService
     */
    private $roleService;
    /**
     * @var \App\Services\Admin\PermissionService
     */
    private $permissionService;

    public function __construct(RoleService $roleService, PermissionService $permissionService)
    {
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getRoles()
    {
        return $this->roleService->select(['id', 'name', 'label']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getPermissions()
    {
        return $this->permissionService->select(['id', 'name', 'label']);
    }

    /**
     * @param $request
     */
    public function assignRolePermission($request)
    {
        $with = 'permissions';
        $where = [
            'attribute' => 'name',
            'value' => $request->role
        ];

        /** @var \App\Models\Admin\Role $role */
        $role = $this->roleService->findByWithRelation($where, $with);
        $role->permissions()->detach();

        foreach ($request->permissions as $permission_name) {
            $permission = $this->permissionService->findPermissionBy('name', $permission_name);
            $role->givePermissionTo($permission);
        }
    }
}