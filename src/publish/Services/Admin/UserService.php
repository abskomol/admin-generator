<?php

namespace App\Services\Admin;

use App\Repositories\Admin\UserRepository;
use App\Services\BaseService;
use App\User;
use Illuminate\Support\Facades\Input;

class UserService extends BaseService
{
    /**
     * @var \App\Repositories\Admin\UserRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Admin\UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \App\Repositories\Repository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Update user info
     *
     * @param array $input
     * @param       $id
     *
     * @return mixed
     */
    public function update(array $input, $id)
    {
        $user = $this->find($id);
        $user->update($input);
        $user->roles()->detach();

        foreach (Input::get('roles') as $role) {
            $user->assignRole($role);
        }

        return $user;
    }

    /**
     * Find role by id with relation
     *
     * @param $id
     * @param $relation
     *
     * @return \App\User
     */
    public function findByIdWithRelation($id, $relation)
    {
        return $this->repository->with($relation)->findBy('id', $id);
    }

    /**
     * Ger use roles
     *
     * @param \App\User $user
     *
     * @return array
     */
    public function getUserRoles(User $user)
    {
        if (!$user->roles) {
            return [];
        }

        $user_roles = [];

        foreach ($user->roles as $role) {
            $user_roles[] = $role->name;
        }

        return $user_roles;
    }
}
