<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\AdminService;
use Illuminate\Http\Request;
use Session;

class AdminController extends Controller
{
    /**
     * @var \App\Services\Admin\AdminService
     */
    private $service;

    /**
     * @param \App\Services\Admin\AdminService $service
     */
    public function __construct(AdminService $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    /**
     * Display given permissions to role.
     */
    public function getGiveRolePermissions()
    {
        $roles = $this->service->getRoles();
        $permissions = $this->service->getPermissions();

        return view('admin.permissions.role-give-permissions', compact('roles', 'permissions'));
    }

    /**
     * Store given permissions to role.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postGiveRolePermissions(Request $request)
    {
        $this->validate($request, ['role' => 'required', 'permissions' => 'required']);
        $this->service->assignRolePermission($request);


        Session::flash('flash_message', 'Permission granted!');

        return redirect('admin/roles');
    }
}
