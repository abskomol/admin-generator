<?php

use Illuminate\Database\Seeder;

class AdminGeneratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create user
        $user = config('auth.providers.users.model');
        $user::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt(123456)
        ]);

        // Create role
        $role = \App\Models\Admin\Role::create([
            'name' => 'admin',
            'label' => 'Administrator'
        ]);

        // Create permission
        $permission = \App\Models\Admin\Permission::create([
            'name' => 'admin_generator',
            'label' => 'Admin generator'
        ]);

        $role->givePermissionTo($permission);
    }
}
