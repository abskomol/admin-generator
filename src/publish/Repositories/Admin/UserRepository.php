<?php

namespace App\Repositories\Admin;

use App\Repositories\Repository;
use App\User;

class UserRepository extends Repository
{

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Filter data based on user input
     *
     * @param array $filter
     * @param       $query
     */
    public function filterData(array $filter, $query)
    {

    }

    public function with($relation)
    {
        $this->model->with($relation);
        return $this;
    }
}