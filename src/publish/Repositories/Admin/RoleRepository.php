<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Role;
use App\Repositories\Repository;

class RoleRepository extends Repository
{

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return Role::class;
    }

    /**
     * Filter data based on user input
     *
     * @param array $filter
     * @param       $query
     */
    public function filterData(array $filter, $query)
    {
        //
    }

    /**
     * @param $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function get($columns)
    {
        return $this->getQuery()->get($columns);
    }

    /**
     * @param array $with
     *
     * @return mixed
     */
    public function getQueryWith(array $with)
    {
        return $this->model->with($with);
    }
}