<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Permission;
use App\Repositories\Repository;

class PermissionRepository extends Repository
{

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return Permission::class;
    }

    /**
     * Filter data based on user input
     *
     * @param array $filter
     * @param       $query
     */
    public function filterData(array $filter, $query)
    {

    }

    /**
     * @param $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function get($columns)
    {
        return $this->getQuery()->get($columns);
    }
}