@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Generator</div>
                    <div class="panel-body">

                        <form class="form-horizontal" method="post" action="{{ url('/admin/generator') }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="crud_name" class="col-md-4 control-label">Crud Name:</label>

                                <div class="col-md-6">
                                    <input type="text" name="crud_name" class="form-control" id="crud_name"
                                           placeholder="Posts" required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="controller_namespace" class="col-md-4 control-label">Namespace:</label>

                                <div class="col-md-6">
                                    <input type="text" name="namespace" class="form-control" id="namespace"
                                           placeholder="Admin">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="route" class="col-md-4 control-label">Want to add route?</label>

                                <div class="col-md-6">
                                    <select name="route" class="form-control" id="route">
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="sd" class="col-md-4 control-label">Want to add soft delete?</label>

                                <div class="col-md-6">
                                    <select name="sd" class="form-control" id="sd">
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                </div>
                            </div>

                            <hr>

                            <table class="table table-fields table-bordered">
                                <thead>
                                <tr>
                                    <th>Field Name</th>
                                    <th>DB Type</th>
                                    <th>Required?</th>
                                    <th>Key</th>
                                    <th>Foreign</th>
                                    <th>Filterable?</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody class="entry">
                                <tr>
                                    <td><input class="form-control" name="fields[]" type="text" placeholder="Field name"
                                               required="true"></td>
                                    <td>
                                        <select name="fields_type[]" class="form-control">
                                            <option value="string">string</option>
                                            <option value="char">char</option>
                                            <option value="varchar">varchar</option>
                                            <option value="password">password</option>
                                            <option value="email">email</option>
                                            <option value="date">date</option>
                                            <option value="datetime">datetime</option>
                                            <option value="time">time</option>
                                            <option value="timestamp">timestamp</option>
                                            <option value="text">text</option>
                                            <option value="mediumtext">mediumtext</option>
                                            <option value="longtext">longtext</option>
                                            <option value="json">json</option>
                                            <option value="jsonb">jsonb</option>
                                            <option value="binary">binary</option>
                                            <option value="number">number</option>
                                            <option value="integer">integer</option>
                                            <option value="bigint">bigint</option>
                                            <option value="mediumint">mediumint</option>
                                            <option value="tinyint">tinyint</option>
                                            <option value="smallint">smallint</option>
                                            <option value="boolean">boolean</option>
                                            <option value="decimal">decimal</option>
                                            <option value="double">double</option>
                                            <option value="float">float</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="fields_required[]" class="form-control">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="keys[]" class="form-control">
                                            <option value=""></option>
                                            <option value="index">Index</option>
                                            <option value="unique">Unique</option>
                                            <option value="unsigned">Unsigned</option>
                                        </select>
                                    </td>

                                    <td>
                                        <input class="form-control" name="foreign_keys[]" type="text"
                                               placeholder="id#users#cascade">
                                    </td>

                                    <td>
                                        <select name="filters[]" class="form-control">
                                            <option value="">No</option>
                                            <option value="like">"%LIKE%"</option>
                                            <option value="=">"="</option>
                                        </select>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-danger btn-remove"><span
                                                    class="glyphicon glyphicon-minus"></span></button>
                                    </td>
                                </tr>
                                </tbody>

                                <tfoot>
                                <tr>
                                    <td colspan="6" class="text-center" style="border: none">
                                        <button class="btn btn-sm btn-success btn-add">ADD NEW FIELD</button>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                            <p class="help text-center">It will automatically assume form fields based on the migration
                                field type.</p>
                            <br>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary pull-right" name="generate">Generate
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- Scripts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.btn-add', function (e) {
                e.preventDefault();

                let tableFields = $('.table-fields'),
                        currentEntry = $('.entry').first(),
                        newEntry = $(currentEntry.clone()).appendTo(tableFields);
                newEntry.find('input').val('');
            }).on('click', '.btn-remove', function (e) {
                if ($('.entry').children().length === 1) {
                    return false;
                }

                $(this).parents('.entry:first').remove();

                e.preventDefault();
                return false;
            });

        });
    </script>

@endsection