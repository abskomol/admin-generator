<?php

namespace Tirjok\AdminGenerator;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use File;

class AdminCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin-generator:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the Admin Generator.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Publishing the assets");
        $this->call('vendor:publish', ['--provider' => 'Tirjok\AdminGenerator\AdminGeneratorServiceProvider']);

        $this->info("Dumping the composer autoload");
        (new Process('composer dump-autoload'))->run();

        $this->info("Migrating the database tables into your application");
        $this->call('migrate');

        $this->info("Adding the routes");

        $routeFile = app_path('Http/routes.php');
        if (\App::VERSION() >= '5.3') {
            $routeFile = base_path('routes/web.php');
        }


        $routes =
            <<<EOD
Route::group(['prefix' => 'admin', 'middleware' => ['web', 'auth']], function () {
    Route::get('/', 'Admin\\AdminController@index');
    Route::get('/give-role-permissions', 'Admin\\AdminController@getGiveRolePermissions');
    Route::post('/give-role-permissions', 'Admin\\AdminController@postGiveRolePermissions');
    Route::resource('/roles', 'Admin\\RolesController');
    Route::resource('/permissions', 'Admin\\PermissionsController');
    Route::resource('/users', 'Admin\\UsersController');
});
EOD;

        File::append($routeFile, "\n" . $routes);

        $this->info("Generating the authentication scaffolding");
        $this->call('make:auth');

        $this->info("Overriding the AuthServiceProvider");
        $contents = File::get(__DIR__ . '/publish/Providers/AuthServiceProvider.php');
        File::put(app_path('Providers/AuthServiceProvider.php'), $contents);

        $this->info("Seeding Admin generator data");
        $this->call('db:seed', ['--class' => 'AdminGeneratorSeeder']);

        $this->info("Successfully installed Tirjok Admin Generator!");
    }
}